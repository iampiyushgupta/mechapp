package in.codetrack.alicorn.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import in.codetrack.alicorn.Model.Booking;

import in.codetrack.alicorn.R;


public class OrderAdapter extends FirestoreRecyclerAdapter<Booking, OrderAdapter.OrderHolder> {
    private OnOrderClickListener listener;
    public OrderAdapter(@NonNull FirestoreRecyclerOptions<Booking> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull OrderHolder orderHolder, int i, @NonNull Booking booking) {
        orderHolder.customerName.setText(booking.getName());
        orderHolder.customerVehicleType.setText(booking.getServiceName());
        orderHolder.customerAddress.setText(booking.getAddressString());
        orderHolder.orderTime.setText(getFormattedTimeStamp(booking.getBookingTimeStamp()));
        if (booking.getAmount() != null) {
            orderHolder.billAmount.setText(String.valueOf(booking.getAmount()));
        }
        if (booking.getVehicleType().equals("2")) {
            orderHolder.customerVehicle.setImageResource(R.drawable.two2);
        } else {
            orderHolder.customerVehicle.setImageResource(R.drawable.four2);
        }
    }

    @NonNull
    @Override
    public OrderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_layout, parent, false);
        return new OrderHolder(v);
    }

    class OrderHolder extends RecyclerView.ViewHolder {

        TextView customerName;
        ImageView customerVehicle;
        TextView customerVehicleType;
        TextView customerAddress;
        TextView billAmount;
        Button callNow;
        TextView orderTime;

        public OrderHolder(@NonNull View itemView) {
            super(itemView);
            customerName = itemView.findViewById(R.id.customer_name);
            customerVehicle = itemView.findViewById(R.id.customer_vehicle_wheels);
            customerVehicleType = itemView.findViewById(R.id.customer_vehicle_type);
            customerAddress = itemView.findViewById(R.id.customer_address);
            billAmount = itemView.findViewById(R.id.customer_bill_amount);
            callNow = itemView.findViewById(R.id.call_customer);
            orderTime = itemView.findViewById(R.id.order_date_time);


            callNow.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onOrderClick(getSnapshots().getSnapshot(position), position);
                    }
                }
            });
        }
    }

    public String getFormattedTimeStamp(Date bookingDate) {
        StringBuilder date = new StringBuilder();

        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat("E, dd MMMM yyyy");
        date.append(formatter.format(bookingDate));
        return date.toString();
    }

    public interface OnOrderClickListener {
        void onOrderClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnOrderClickListener(OnOrderClickListener listener) {
        this.listener = listener;
    }
}
