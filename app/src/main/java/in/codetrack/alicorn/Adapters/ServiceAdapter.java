package in.codetrack.alicorn.Adapters;

import android.app.Dialog;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import in.codetrack.alicorn.Model.Service;
import in.codetrack.alicorn.R;
import in.codetrack.alicorn.four_one;

import java.util.List;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.MyViewHolder> {
    List<Service> dataset;
    four_one ctx;
    private OnServiceClickListener listener;

    @NonNull
    @Override
    public ServiceAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.servicecardview, parent, false);
//        view.setOnClickListener(mOnClickListener);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceAdapter.MyViewHolder holder, int position) {
        Service service = dataset.get(position);
        holder.serviceName.setText(service.getServiceName());
        holder.crossedPrice.setText(service.getCrossedPrice());
        holder.newPrice.setText(service.getNewPrice());
        if (service.getWheels().equals("2")) {
            holder.vehicleImage.setImageResource(R.drawable.two2);
        } else {
            holder.vehicleImage.setImageResource(R.drawable.four2);
        }
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView crossedPrice, newPrice, serviceName;
        public ImageView vehicleImage;
        public Button bookNow,fourDetail;
        public TextView text;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            serviceName = itemView.findViewById(R.id.serviceName);
            newPrice = itemView.findViewById(R.id.newprice);
            crossedPrice = itemView.findViewById(R.id.previous_price);
            vehicleImage = itemView.findViewById(R.id.thumbnail);
            bookNow = (Button) itemView.findViewById(R.id.book_now);
            fourDetail= itemView.findViewById(R.id.fourDetail);
            bookNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onServiceClick(position);
                    }
                }
            });

            fourDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Dialog dialog = new Dialog(view.getContext());
                    dialog.setContentView(R.layout.four_detail_service_list);
                    dialog.setTitle("Title...");
                    text =  dialog.findViewById(R.id.first);
                    dialog.show();
                }
            });
            crossedPrice.setPaintFlags(crossedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    public ServiceAdapter(four_one ctx, List<Service> dataset) {
        this.ctx = ctx;
        this.dataset = dataset;
    }

    public interface OnServiceClickListener {
        void onServiceClick(int position);
    }

    public void setOnServiceClick(OnServiceClickListener listener) {
        this.listener = listener;
    }

}
