package in.codetrack.alicorn;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import in.codetrack.alicorn.Model.Address;
import in.codetrack.alicorn.Model.Booking;

import in.codetrack.alicorn.Model.Coupon;
import in.codetrack.alicorn.Model.CouponData;
import in.codetrack.alicorn.R;

import java.util.Date;
import java.util.Map;

public class BookingDetails extends AppCompatActivity {
    FirebaseFirestore db;
    protected Booking bookingDetail;
    FirebaseAuth mAuth;
    FirebaseUser userDetails;
    EditText name, phone, landmark, addressLine1, addressLine2, pincode;
    Button bookNow;
    Address userAddress;
    Coupon allCoupons;
    DocumentReference docRef;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Phone = "phoneKey";
    int PRIVATE_MODE = 0;
    SharedPreferences sharedPreferences;



    public BookingDetails() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.activity_booking_details);
        db = FirebaseFirestore.getInstance();
        bookingDetail = new Booking();
        GoogleSignInAccount user = GoogleSignIn.getLastSignedInAccount(this);

        // get data from extra
        bookingDetail.setServiceName(getIntent().getStringExtra("serviceType"));
        bookingDetail.setVehicleType(getIntent().getStringExtra("vehicleType"));
        bookingDetail.setAmount(getIntent().getStringExtra("amount"));


        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        landmark = findViewById(R.id.landmark);
        bookNow = findViewById(R.id.bookNow);
        addressLine1 = findViewById(R.id.addressLine1);
        addressLine2 = findViewById(R.id.addressLine2);
        pincode = findViewById(R.id.pincode);

        phone.setText(getPhoneNUmberFromSharedPreference());


        name.setText(user.getDisplayName());
        checkForCoupons();
    }


    public String getPhoneNUmberFromSharedPreference() {
        Context _context  = getApplicationContext();
        sharedPreferences = _context.getSharedPreferences(MyPREFERENCES, PRIVATE_MODE);
        return sharedPreferences.getString(Phone, "");
    }


    public void checkForCoupons() {
        userDetails = mAuth.getCurrentUser();

        Log.e("PRINT_U_DET", userDetails.getUid());
        docRef = db.collection("available_coupons").document(userDetails.getUid());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.e("PRINT_doc", document.toString());
                        allCoupons = document.toObject(Coupon.class);
                        if (allCoupons.coupon.size() > 0) {
                            displayToast("You will get free Air filter on this order");
                        }
                    }
                }
            }
        });
    }

    public void displayToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void confirmBooking(final View view) {
        bookingDetail.setName(name.getText().toString());
        bookingDetail.setPhone(phone.getText().toString());

        userAddress = new Address(addressLine1.getText().toString(),
                addressLine2.getText().toString(), pincode.getText().toString(), landmark.getText().toString());

        bookingDetail.setAddress(userAddress);
        bookingDetail.setBookingTimeStamp(new Date());



        // validations

        if (name.getText().toString().trim().matches("")) {
            Toast.makeText(this, "Please enter the name", Toast.LENGTH_SHORT).show();
            return;
        }

        if (phone.getText().toString().trim().matches("")) {
            Toast.makeText(this, "Please enter the phone", Toast.LENGTH_SHORT).show();
            return;
        }

        if (landmark.getText().toString().trim().matches("")) {
            Toast.makeText(this, "Please enter the landmark", Toast.LENGTH_SHORT).show();
            return;
        }

        if (allCoupons != null && allCoupons.coupon.size() > 0) {
            Map.Entry<String, CouponData> entry = allCoupons.coupon.entrySet().iterator().next();
            final String couponId = allCoupons.coupon.remove(entry.getKey()).getCouponId();
            docRef.set(allCoupons)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            displayToast("Hurray! You will get a free Air filter and ₹50 off on this order");
                            bookingDetail.setAmount(Integer.toString(Integer.parseInt(bookingDetail.getAmount()) - 50));
                            bookingDetail.coupon = couponId;
                            Log.d("COUPON_ADDED", "Coupon added successfully!");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("COUPON_ADDING_ERROR", "Unable to add coupon", e);
                        }
                    });
        }




        // firebase call with the data available with us
        db.collection("booking").add(bookingDetail)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
//                        Log.d("success", "DocumentSnapshot added with ID: " + documentReference.getId());
                        name.setText("");
                        phone.setText("");
                        landmark.setText("");
                        addressLine1.setText("");
                        addressLine2.setText("");
                        pincode.setText("");
                        Toast.makeText(view.getContext(), "Booking done" , Toast.LENGTH_SHORT ).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("failure", "Error adding document", e);
                    }
                });

        Intent i = new Intent(this, confirm.class);
        startActivity(i);

    }
}
