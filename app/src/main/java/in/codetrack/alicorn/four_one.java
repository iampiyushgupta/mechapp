package in.codetrack.alicorn;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import in.codetrack.alicorn.Adapters.ServiceAdapter;
import in.codetrack.alicorn.Model.Service;

import in.codetrack.alicorn.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class four_one extends Fragment {
    private RecyclerView recyclerView;
    private ServiceAdapter serviceAdapter;
    private TextView text;
    ArrayList<Service> dataset = new ArrayList<Service>();


    Button bookButton,fourDetail;
    public four_one() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_four_one, container, false);
        bookButton = view.findViewById(R.id.book);
        fourDetail=view.findViewById(R.id.fourDetail);
        recyclerView= view.findViewById(R.id.recycler_view);

        dataset = new ArrayList<Service>();

        dataset.add(new Service("4000", "2099", "Hatchback", "4"));
        dataset.add(new Service("5500", "2399", "Sedan", "4"));
        dataset.add(new Service("8000", "3699", "SUV", "4"));

        serviceAdapter = new ServiceAdapter(this, dataset);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(serviceAdapter);

        serviceAdapter.setOnServiceClick(new ServiceAdapter.OnServiceClickListener() {
            @Override
            public void onServiceClick(int position) {
                Service selectedService = dataset.get(position);

                Intent i = new Intent(getActivity(), BookingDetails.class);
                i.putExtra("serviceType", selectedService.getServiceName());
                i.putExtra("vehicleType", selectedService.getWheels());
                i.putExtra("amount", selectedService.getNewPrice());
                startActivity(i);
            }
        });


        return view;

    }
}
