package in.codetrack.alicorn;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.UUID;

import in.codetrack.alicorn.Model.Coupon;
import in.codetrack.alicorn.Model.CouponCodeCouponMapping;
import in.codetrack.alicorn.Model.CouponData;
import in.codetrack.alicorn.Model.LoginModel;

public class SignUp extends AppCompatActivity {

    FirebaseFirestore db;
    protected LoginModel bookingDetail;


    EditText phone;
    EditText referralCode;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Phone = "phoneKey";
    int PRIVATE_MODE = 0;

    public SignUp() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (isSharedPreferenceSet()) {
            // when we find a string with phone number in shared preference, skip the current activity and display the main activity
            startMainActivity();
        } else {
            setContentView(R.layout.activity_sign_up);
        }

        db = FirebaseFirestore.getInstance();
        bookingDetail = new LoginModel();
        phone = findViewById(R.id.phone);
        referralCode = findViewById(R.id.referralCode);

    }

    public boolean isSharedPreferenceSet() {
        Context _context  = getApplicationContext();
        sharedpreferences = _context.getSharedPreferences(MyPREFERENCES, PRIVATE_MODE);
        return sharedpreferences.getString(Phone, "") != "";
    }

    @Override
    protected void onResume()
    {
        if (isSharedPreferenceSet()) {
            this.finishAffinity();
        }
        super.onResume();
    }

    public void confirmBooking(final View view) {
        bookingDetail.setPhone(phone.getText().toString());
        bookingDetail.setUser();

        if (phone.getText().toString().trim().matches("")) {
            Toast.makeText(this, "Please enter the phone", Toast.LENGTH_SHORT).show();
            return;
        }

        //verify coupon
        if (referralCode.getText().toString().trim() != "" && referralCode.getText().toString().trim() != null && !referralCode.getText().toString().trim().isEmpty()) {
            if (bookingDetail.getUser().substring(0, 5).toUpperCase() == referralCode.getText().toString().trim().substring(0, 5).toUpperCase()) {
                displayToast("You cannot use your own coupon code");
                return;
            }
            DocumentReference docRef = db.collection("couponcode_coupon_mapping").document(referralCode.getText().toString().trim());
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                             CouponCodeCouponMapping mapping = document.toObject(CouponCodeCouponMapping.class);
                             // add coupon to both the parties
                            // get current user id from
                            Log.e("PRINTING_USER_DATA", bookingDetail.getUser());
                            CouponData couponForNewUser = new CouponData(mapping.couponId, mapping.sourceId);
                            CouponData couponForOldUser = new CouponData(mapping.couponId, bookingDetail.getUser());

                            Coupon newUserCoupon = new Coupon(couponForNewUser);

                            validateCouponAddition(newUserCoupon, couponForOldUser, mapping);
                        } else {
                            displayToast("Referral code not valid");
                        }
                    } else {
                        displayToast("Unable to reach servers");
                    }
                }
            });
        } else {
            sendData();
        }
    }


    public void validateCouponAddition(final Coupon newUserCoupon, final CouponData oldUserCouponData, final CouponCodeCouponMapping referral) {
        // check if the coupon already exists for the new user i.e the user has already used this coupon
        DocumentReference availableCoupons = db.collection("available_coupons").document(bookingDetail.getUser());
        availableCoupons.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        displayToast("You have already availed this coupon");
                    } else {
                        // if the user is a new user and it does not have any coupon, add this coupon and let it work as expected
                        addCouponToNewUser(newUserCoupon);
                        addCouponToReferrer(oldUserCouponData, referral);
                        sendData();
                    }
                } else {
                    Log.d("COUPON_ERROR", "unable to add coupon to your account, please try again later", task.getException());
                }
            }
        });
    }

    public void addCouponToNewUser(Coupon newUserCoupon) {
        db.collection("available_coupons").document(bookingDetail.getUser()).set(newUserCoupon)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("COUPON_ADDED", "Coupon added successfully!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("COUPON_ADDING_ERROR", "Unable to add coupon", e);
                    }
                });

    }

    public void addCouponToReferrer(final CouponData oldUserCouponData, CouponCodeCouponMapping referrer) {

        final DocumentReference docRef = db.collection("available_coupons").document(referrer.sourceId);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.e("PRINT_doc", document.toString());
                        Coupon allCoupons = document.toObject(Coupon.class);
                        allCoupons.coupon.put((UUID.randomUUID().toString()), oldUserCouponData);
                        docRef.set(allCoupons)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        displayToast("Coupon added, you can use it in your next order");
                                        Log.d("COUPON_ADDED", "Coupon added successfully!");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.e("COUPON_ADDING_ERROR", "Unable to add coupon", e);
                                    }
                                });
                    }
                } else {
                    Log.e("PRINT_REF_COUPON", "not successful");
                }
            }
        });
    }

    public void displayToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void sendData() {
        // firebase call with the data available with us
        db.collection("login").add(bookingDetail)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
//                        Log.d("success", "DocumentSnapshot added with ID: " + documentReference.getId());
                        phone.setText("");
                        startMainActivity();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("failure", "Error adding number", e);
                    }
                });
        String ph  = phone.getText().toString();
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Phone,ph);
        editor.commit();
    }

    public void startMainActivity() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
