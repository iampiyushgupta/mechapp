package in.codetrack.alicorn.Fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import in.codetrack.alicorn.R;
import in.codetrack.alicorn.four_one;
import in.codetrack.alicorn.four_three;
import in.codetrack.alicorn.four_two;

import java.util.ArrayList;
import java.util.List;


public class Four_wheeler extends androidx.fragment.app.Fragment {

    TabLayout tabs;
    private int[] tabIcons = {
            R.drawable.four,
            R.drawable.four,
            R.drawable.four,
//            R.drawable.four,
//            R.drawable.four,
//            R.drawable.four
    };



    public Four_wheeler() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_four_wheeler,container, false);
        // Setting ViewPager for each Tabs
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        tabs = (TabLayout) view.findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);
        setupTabIcons();

        return view;
    }


    private void setupTabIcons() {

        tabs.getTabAt(0).setIcon(tabIcons[0]);
        tabs.getTabAt(1).setIcon(tabIcons[1]);
        tabs.getTabAt(2).setIcon(tabIcons[2]);
//        tabs.getTabAt(3).setIcon(tabIcons[3]);
//        tabs.getTabAt(4).setIcon(tabIcons[4]);
//        tabs.getTabAt(5).setIcon(tabIcons[5]);
    }

    private void setupViewPager(ViewPager viewPager) {
        Two_wheeler.Adapter adapter = new Two_wheeler.Adapter(getChildFragmentManager());
        adapter.addFragment(new four_one(), "Service");
        adapter.addFragment(new four_two(), "Breakdown");
        adapter.addFragment(new four_three(), "Washing");
        viewPager.setAdapter(adapter);
    }
    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
