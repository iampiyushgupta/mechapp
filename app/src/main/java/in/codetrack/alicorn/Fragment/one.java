package in.codetrack.alicorn.Fragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import in.codetrack.alicorn.BookingDetails;
import in.codetrack.alicorn.Model.CouponCodeCouponMapping;
import in.codetrack.alicorn.R;
import in.codetrack.alicorn.confirm;

import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * A simple {@link Fragment} subclass.
 */
public class one extends Fragment {

    Button bookNow,twoDetail,shareBtn;
    TextView crossedPrice;
    private View mMainView;
    private TextView text;
    FirebaseAuth mAuth;
    FirebaseFirestore db;
    String couponCode;

    public one() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mMainView = inflater.inflate(R.layout.fragment_one, container, false);
        crossedPrice = mMainView.findViewById(R.id.previous_price);
        crossedPrice.setPaintFlags(crossedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        bookNow = mMainView.findViewById(R.id.book);
        shareBtn=mMainView.findViewById(R.id.shareBtn);
        twoDetail = mMainView.findViewById(R.id.twoDetail);
        bookNow.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(getActivity(), BookingDetails.class);
                i.putExtra("serviceType", "Standard Service");
                i.putExtra("vehicleType", "2");
                i.putExtra("amount", "599");
                startActivity(i);
            }
        });

        twoDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.two_detail_service);
                dialog.setTitle("Title...");
                text =  dialog.findViewById(R.id.first);
                dialog.show();
            }
        });
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                String shareBody =
//                        "Hey check out this app at: https://play.google.com/store/apps/details?id=in.codetrack.alicorn&hl=en, Share and get free engine oil,\n" +
//                                "free air filter,\n" +
//                                "free brake shoe \n" +
//                                "and complete doorstep service for your bike only at Rs 599/-\n" +
//                                "\n" +"use this coupon "+getUserCoupon()+" to get additional 50 rs off..";
//                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Hey, check this app out");
//                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
//                startActivity(Intent.createChooser(sharingIntent, "Share Alicorn"));
                Intent i = new Intent(getActivity(), confirm.class);

                startActivity(i);
            }
        });
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        setCouponCode();
        return mMainView;
        // Inflate the layout for this fragment

    }


    public String getUserCoupon() {
        // get the userd id
        String coupon = "6WZDjbimC2IvmYT01UOv".toUpperCase();

        String couponInitials = coupon.substring(0, 4);
        String userIdInitials = mAuth.getUid().substring(0, 5).toUpperCase();

        String generatedCoupon = userIdInitials + couponInitials;
        insertCoupon(generatedCoupon, coupon, mAuth.getUid());

        return generatedCoupon;
    }
    public void insertCoupon(String generatedCoupon, String coupon, String userId) {
        CouponCodeCouponMapping couponMapping = new CouponCodeCouponMapping();
        couponMapping.setCouponId(coupon);
        couponMapping.setSourceId(userId);

        db.collection("couponcode_coupon_mapping").document(generatedCoupon).set(couponMapping)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("COUPON_ADDED", "Coupon added successfully!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("COUPON_ADDING_ERROR", "Unable to add coupon", e);
                    }
                });
    }
    public void setCouponCode() {
        couponCode = getUserCoupon();
    }
}
