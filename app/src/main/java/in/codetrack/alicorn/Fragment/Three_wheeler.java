package in.codetrack.alicorn.Fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.codetrack.alicorn.R;

public class Three_wheeler extends androidx.fragment.app.Fragment {


    public Three_wheeler() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_three_wheeler, container, false);
    }

}
