package in.codetrack.alicorn.Fragment;


import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import in.codetrack.alicorn.BookingDetails;

import in.codetrack.alicorn.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class three extends Fragment {

    TextView crossedPrice;
    private View mMainView;
    Button bookNow;

    public three() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mMainView = inflater.inflate(R.layout.fragment_three, container, false);
        crossedPrice = mMainView.findViewById(R.id.previous_price);
        crossedPrice.setPaintFlags(crossedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        bookNow = mMainView.findViewById(R.id.book);
        bookNow.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(getActivity(), BookingDetails.class);
                i.putExtra("serviceType", "Washing");
                i.putExtra("vehicleType", "2");
                i.putExtra("amount", "50");
                startActivity(i);
            }
        });

        return mMainView;
    }

}
