package in.codetrack.alicorn.Model;

import java.util.Date;

public class CouponData {
    Boolean available;
    String couponId;
    Date redeemedOn;
    String referrer;

    public CouponData() {

    }

    public CouponData(String couponId, String referrer) {
        this.couponId = couponId;
        this.referrer = referrer;
        this.available = true;
        this.redeemedOn = new Date();
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public Date getRedeemedOn() {
        return redeemedOn;
    }

    public void setRedeemedOn(Date redeemedOn) {
        this.redeemedOn = redeemedOn;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }


}
