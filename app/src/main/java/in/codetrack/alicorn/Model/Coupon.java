package in.codetrack.alicorn.Model;

import java.util.HashMap;
import java.util.UUID;

public class Coupon {
    public HashMap<String, CouponData> coupon;

    public Coupon() {

    }

    public Coupon(String couponId, CouponData couponData) {
        coupon = new HashMap<String, CouponData>();
        coupon.put(couponId, couponData);
    }


    public HashMap<String, CouponData> getCoupon() {
        return coupon;
    }

    public void setCoupon(HashMap<String, CouponData> coupons) {
        this.coupon = coupons;
    }


    public Coupon(CouponData couponData) {
        coupon = new HashMap<String, CouponData>();
        coupon.put(UUID.randomUUID().toString(), couponData);
    }
}
