package in.codetrack.alicorn.Model;

public class Service {
    String crossedPrice;
    String newPrice;
    String serviceName;
    String wheels;

    public Service() {

    }

    public Service(String crossedPrice, String newPrice, String serviceName, String wheels) {
        this.crossedPrice = crossedPrice;
        this.newPrice = newPrice;
        this.serviceName = serviceName;
        this.wheels = wheels;
    }

    public String getCrossedPrice() {
        return crossedPrice;
    }

    public void setCrossedPrice(String crossedPrice) {
        this.crossedPrice = crossedPrice;
    }

    public String getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(String newPrice) {
        this.newPrice = newPrice;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }


    public String getWheels() {
        return wheels;
    }

    public void setWheels(String wheels) {
        this.wheels = wheels;
    }

    @Override
    public String toString() {
        return "Crossed Price: " + crossedPrice + ", New Price: " + newPrice + ", Service Name: " + serviceName + ", Wheels: " + wheels;
    }
}
