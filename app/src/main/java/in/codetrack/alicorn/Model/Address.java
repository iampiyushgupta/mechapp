package in.codetrack.alicorn.Model;

import androidx.annotation.NonNull;

public class Address {
    String addressLine1, addressLine2, landmark, pincode;

    public Address() {

    }

    public Address(String addressLine1, String addressLine2, String pincode, String landmark) {
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.landmark = landmark;
        this.pincode = pincode;
    }

    @NonNull
    @Override
    public String toString() {
        return addressLine1 + ", " + addressLine2 + ", " + pincode + ", Landmark: " + landmark;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

}
