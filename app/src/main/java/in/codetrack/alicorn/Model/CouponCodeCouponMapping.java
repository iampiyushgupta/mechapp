package in.codetrack.alicorn.Model;

public class CouponCodeCouponMapping {

    public String couponId;
    public String sourceId;


    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }
}
