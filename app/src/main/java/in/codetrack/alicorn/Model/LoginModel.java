package in.codetrack.alicorn.Model;

import com.google.firebase.auth.FirebaseAuth;

public class LoginModel {
    private String phone;
    private FirebaseAuth mAuth;
    private String user;

    public LoginModel() {

    }

    public void setUser() {
        mAuth = FirebaseAuth.getInstance();
        this.user = mAuth.getCurrentUser().getUid();
    }

    public String getUser() {
        return user;
    }

    public LoginModel(String phone) {
        this.phone = phone;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
