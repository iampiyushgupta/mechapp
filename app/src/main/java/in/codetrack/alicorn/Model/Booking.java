package in.codetrack.alicorn.Model;

import java.util.Date;

public class Booking {
    private String serviceName;
    private String vehicleType;
    private String name;
    private String phone;
    private Address address;
    private String amount;
    public String coupon;

    Date bookingTimeStamp;

    public Booking() {

    }

    public Booking(String serviceName, String vehicleType, String name, String phone, Date bookingTimeStamp, String amount) {
        this.serviceName = serviceName;
        this.vehicleType = vehicleType;
        this.name = name;
        this.phone = phone;
        this.bookingTimeStamp = bookingTimeStamp;
        this.amount = amount;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Address getAddress() {
        return address;
    }

    public String getAddressString() {
        return address.toString();
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Date getBookingTimeStamp() {
        return bookingTimeStamp;
    }

    public void setBookingTimeStamp(Date bookingTimeStamp) {
        this.bookingTimeStamp = bookingTimeStamp;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
