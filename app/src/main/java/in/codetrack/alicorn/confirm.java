package in.codetrack.alicorn;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.File;
import java.io.FileOutputStream;

import in.codetrack.alicorn.Model.CouponCodeCouponMapping;
import in.codetrack.alicorn.R;

public class confirm extends AppCompatActivity {

    Button okButton;
    TextView mCode;
    TextView userCouponCode;
    Button googleLogin;
    FirebaseAuth mAuth;
    FirebaseFirestore db;
    String couponCode;
    ImageView imgShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        okButton = findViewById(R.id.ok);
        mCode= findViewById(R.id.dev);
        userCouponCode= findViewById(R.id.sharecoupontext);
        imgShare=findViewById(R.id.sharepost);
//        okButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getBaseContext(), Login.class));
//                finish();
//            }
//        });
        mCode.setText(
                Html.fromHtml(
                        getString(R.string.codelink)));
        mCode.setMovementMethod(LinkMovementMethod.getInstance());
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        setCouponCode();
        userCouponCode.setText(this.couponCode);
    }


    public String getUserCoupon() {
        // get the userd id
        String coupon = "6WZDjbimC2IvmYT01UOv".toUpperCase();

        String couponInitials = coupon.substring(0, 4);
        String userIdInitials = mAuth.getUid().substring(0, 5).toUpperCase();

        String generatedCoupon = userIdInitials + couponInitials;
        insertCoupon(generatedCoupon, coupon, mAuth.getUid());

        return generatedCoupon;
    }

    public void insertCoupon(String generatedCoupon, String coupon, String userId) {
        CouponCodeCouponMapping couponMapping = new CouponCodeCouponMapping();
        couponMapping.setCouponId(coupon);
        couponMapping.setSourceId(userId);

        db.collection("couponcode_coupon_mapping").document(generatedCoupon).set(couponMapping)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("COUPON_ADDED", "Coupon added successfully!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("COUPON_ADDING_ERROR", "Unable to add coupon", e);
                    }
                });
    }

    public void setCouponCode() {
        couponCode = getUserCoupon();
    }

    public void copyCouponCode(final View view) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Coupon Code", couponCode);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this, "code copied to clipboard", Toast.LENGTH_SHORT).show();
    }
    public void shareCoupon(final View view) {
//        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//        sharingIntent.setType("text/plain");
        String shareBold ="*Check This for Bike Service On Your Doorstep Only in 599/-"+"*";
        String shareBody ="Hey,\n"+"*"+"Check This for Bike Service On Your Doorstep Only in 599/-"+"*"+"\n\n"+
                "https://play.google.com/store/apps/details?id=in.codetrack.alicorn&hl=en," +"\n\n"+
                        "\nShare and get\nfree engine oil,\n"+
                        "free air filter,\n" +
                        "free brake shoe \n" +
                        "with complete Bike Servicing with washing on your Doorstep\n" +
                        "\n" +"use this coupon code \n"+"*"+getUserCoupon()+"*"+" to get additional 50Rs off..";
//        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Hey, check this app out");
//        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
//        startActivity(Intent.createChooser(sharingIntent, "Share Alicorn"));


        Drawable drawable=imgShare.getDrawable();
        Bitmap bitmap=((BitmapDrawable)drawable).getBitmap();

        try {
            File file = new File(getApplicationContext().getExternalCacheDir(), File.separator +"sharepost.png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
                    BuildConfig.APPLICATION_ID +".provider", file);

            intent.putExtra(Intent.EXTRA_STREAM, photoURI);
            intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setType("image/png");

            startActivity(Intent.createChooser(intent, "Share Alicorn"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
